﻿using System;

namespace OwinSelfHost.Authentication
{
    class UserAuthentication
    {
        private static User registeredUser = new User { Name = "name", Password = "pAss" };

        public bool AuthenticateUser(User user)
        {
            return registeredUser.Name.Equals(user.Name, StringComparison.OrdinalIgnoreCase) && registeredUser.Password.Equals(user.Password);
        }
    }
}
