﻿using System;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Threading;
using System.Security.Principal;
using System.Security.Authentication;

namespace OwinSelfHost.Authentication
{
    class BasicAuthentication : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var isAuthenticationOk = false;

            try
            {
                if (actionContext.Request.Headers.Authorization != null)
                {
                    var authenticationToken = actionContext.Request.Headers.Authorization.Parameter;
                    var decodedAuthenticationToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
                    var user = decodedAuthenticationToken.Split(':');

                    if (user.Length == 2)
                    {
                        var name = user[0];
                        var pass = user[1];

                        if (new UserAuthentication().AuthenticateUser(new User { Name = name, Password = pass }))
                        {
                            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(name), null);
                            isAuthenticationOk = true;
                        }
                    }
                }
            }
            catch { }

            if (!isAuthenticationOk)
            {
                throw new AuthenticationException();
            }
        }
    }
}
