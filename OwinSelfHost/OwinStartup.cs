﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Newtonsoft.Json;
using Owin;
using OwinSelfHost.ExceptionHandlers;
using OwinSelfHost.Validation;

namespace OwinSelfHost
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultCompany",
                routeTemplate: "company/{id}",
                defaults: new { controller = "Company", id = RouteParameter.Optional }
            );

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings.Re‌​ferenceLoopHandling = ReferenceLoopHandling.Ignore;
            config.Filters.Add(new ValidateModelAttribute());
            config.Filters.Add(new ControllerExceptionFilter());
            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());

            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            appBuilder.UseWebApi(config);
        }
    }
}
