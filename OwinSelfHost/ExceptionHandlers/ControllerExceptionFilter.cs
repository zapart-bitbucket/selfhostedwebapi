﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http.Filters;
using CompanyRepository.ExceptionHandlers;

namespace OwinSelfHost.ExceptionHandlers
{
    class ControllerExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var status = HttpStatusCode.InternalServerError;
            var message = String.Empty;

            var exceptionType = actionExecutedContext.Exception.GetType();
            if (exceptionType == typeof(AuthenticationException))
            {
                message = "User authentication failed.";
                status = HttpStatusCode.Unauthorized;
            }
            else if (exceptionType == typeof(DivideByZeroException))
            {
                message = "Internal Server Error: Illegal divide by zero operation.";
                status = HttpStatusCode.InternalServerError;
            }
            else if (exceptionType == typeof(ArgumentOutOfRangeException))
            {
                message = string.Format("Company with id = {0} not found.", (actionExecutedContext.Exception as ArgumentOutOfRangeException).ActualValue);
                status = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(CompanyDtoNullException))
            {
                message = "Usage error: Company object is empty.";
                status = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(CompanyRepositoryCompanyNullException))
            {
                message = "Usage error: Update Company object is empty.";
                status = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(CompanyRepositoryNullException))
            {
                message = "Company repository not found."; 
                status = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(CompanyRepositorySearchNullException))
            {
                message = "Usage error: Search object is empty."; 
                status = HttpStatusCode.NotFound;
            }
            else
            {
                message = "An unexpected error occured. Please try again later.";
                status = HttpStatusCode.ExpectationFailed;
            }

            actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(status, message);

            base.OnException(actionExecutedContext);
        }
    }
}