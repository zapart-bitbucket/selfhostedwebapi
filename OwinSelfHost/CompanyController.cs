﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using CompanyRepository.Models;
using CompanyRepository;
using OwinSelfHost.Authentication;
using CompanyRepository.DBContext;
using System.Web.Http.Description;
using OwinSelfHost.ExceptionHandlers;

namespace OwinSelfHost
{
    public class CompanyController : ApiController
    {
        private CompanyRepositoryService _companyRepositoryService =
            new CompanyRepositoryService(new EFCompanyRepositoryCRUD(new EFCompanyRepository()));

        [ResponseType(typeof(List<CompanyDTO>))]
        [BasicAuthentication]
        [HttpGet]
        public IHttpActionResult Get()
        {
            List<Company> companies = null;
            List<CompanyDTO> companiesDTO = null;

            companies = _companyRepositoryService.CompaniesRepositoryCRUD.GetAllCompanies().ToList();
            if (companies != null)
            {
                companiesDTO = companies.Select( c => new CompanyDTO
                               {
                                    Name = c.Name,
                                    EstablishmentYear = c.EstablishmentYear,
                                    Employees = c.Employees.Select( e => new EmployeeDTO
                                                {
                                                    FirstName = e.FirstName,
                                                    LastName = e.LastName,
                                                    DateOfBirth = e.DateOfBirth,
                                                    JobTitle = e.JobTitle.ToString()
                                                }).ToList()
                               }).ToList();
            }
            else
            {
                return NotFound();
            }

            return Ok(companiesDTO);
        }

        [ResponseType(typeof(CompanyDTO))]
        [Route("company/{id}")]
        [BasicAuthentication]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            CompanyDTO companyDTO = null;

            Company company = _companyRepositoryService.CompaniesRepositoryCRUD.GetById(id);
            if (company != null)
            {
                companyDTO = new CompanyDTO
                {
                    Name = company.Name,
                    EstablishmentYear = company.EstablishmentYear,
                    Employees = company.Employees.Select( e => new EmployeeDTO
                                {
                                    FirstName = e.FirstName,
                                    LastName = e.LastName,
                                    DateOfBirth = e.DateOfBirth,
                                    JobTitle = e.JobTitle.ToString()
                                }).ToList()
                };
            }
            else
            {
                return NotFound();
            }

            return Ok(companyDTO);
        }

        [Route("company/create")]
        [BasicAuthentication]
        [HttpPost]
        public IHttpActionResult Post([FromBody]CompanyDTO companyDTO)
        {
            if( companyDTO == null )
            {
                throw new CompanyDtoNullException();
            }

            var company = new Company
            {
                Name = companyDTO.Name,
                EstablishmentYear = companyDTO.EstablishmentYear,
                Employees = companyDTO.Employees.Select(e => new Employee
                            {
                                FirstName = e.FirstName,
                                LastName = e.LastName,
                                DateOfBirth = e.DateOfBirth,
                                JobTitle = (JobTitleType)Enum.Parse(typeof(JobTitleType), e.JobTitle),

                            }).ToList()
            };
            _companyRepositoryService.CompaniesRepositoryCRUD.AddCompany(company);
            return Ok(new { Id = company.Id });
        }

        [ResponseType(typeof(SearchCompanyDTO))]
        [Route("company/search")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]SearchCompanyDTO searchCompany)
        {
            IEnumerable<Company> companies = null;
            List<CompanyDTO> companiesDTO = null;

            companies = _companyRepositoryService.CompaniesRepositoryCRUD.SearchCompanyRepo(searchCompany);
            if (companies != null)
            {
                companiesDTO = companies.Select(c => new CompanyDTO
                               {
                                   Name = c.Name,
                                   EstablishmentYear = c.EstablishmentYear,
                                   Employees = c.Employees.Select(
                                          e => new EmployeeDTO
                                          {
                                              FirstName = e.FirstName,
                                              LastName = e.LastName,
                                              DateOfBirth = e.DateOfBirth,
                                              JobTitle = e.JobTitle.ToString()
                                          }).ToList()
                               }).ToList();
            }
            else
            {
                return NotFound();
            }

            return Ok(companiesDTO);
        }

        [ResponseType(typeof(void))]
        [Route("company/update/{id}")]
        [BasicAuthentication]
        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]CompanyDTO companyDTO)
        {
            if (companyDTO == null)
            {
                throw new CompanyDtoNullException();
            }

            var company = new Company
            {
                Name = companyDTO.Name,
                EstablishmentYear = companyDTO.EstablishmentYear,
                Employees = companyDTO.Employees.Select(e => new Employee
                            {
                                FirstName = e.FirstName,
                                LastName = e.LastName,
                                DateOfBirth = e.DateOfBirth,
                                JobTitle = (JobTitleType)Enum.Parse(typeof(JobTitleType), e.JobTitle),

                            }).ToList()
            };
            _companyRepositoryService.CompaniesRepositoryCRUD.UpdateCompany(id, company);
            return Ok();
        }

        [Route("company/delete/{id}")]
        [BasicAuthentication]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            _companyRepositoryService.CompaniesRepositoryCRUD.DeleteCompany(id);
            return Ok();
        }

        private void FillOutDataBase()
        {
            using (var context = new CompanyDbContext())
            {
                var newCompanies = new List<Company>()
                {
                    new Company
                    {
                        Name = "Chicago Bulls",
                        EstablishmentYear = 1972,
                        Employees = new List<Employee>()
                        {
                            new Employee{ FirstName="Michael", LastName="Jordan", JobTitle=JobTitleType.Administrator,
                                          DateOfBirth =new DateTime(1968,3,7) },
                            new Employee{ FirstName="Isiah", LastName="Thomas", JobTitle=JobTitleType.Architect,
                                          DateOfBirth =new DateTime(1972,8,22) },
                            new Employee{ FirstName="Scottie", LastName="Pippen", JobTitle=JobTitleType.Manager,
                                          DateOfBirth =new DateTime(1970,3,12) }
                        }
                    },
                    new Company
                    {
                        Name = "IBM",
                        EstablishmentYear = 1935,
                        Employees = new List<Employee>()
                        {
                            new Employee{ FirstName="John", LastName="Thompson", JobTitle=JobTitleType.Administrator,
                                          DateOfBirth =new DateTime(1938,3,7) },
                            new Employee{ FirstName="Jason", LastName="Scott", JobTitle=JobTitleType.Architect,
                                          DateOfBirth =new DateTime(1932,8,22) },
                            new Employee{ FirstName="Crazy", LastName="Horse", JobTitle=JobTitleType.Manager,
                                          DateOfBirth =new DateTime(1930,3,12) }
                        }
                    },
                    new Company
                    {
                        Name = "Microsoft",
                        EstablishmentYear = 1967,
                        Employees = new List<Employee>()
                        {
                            new Employee{ FirstName="Steve", LastName="Ballmer", JobTitle=JobTitleType.Administrator,
                                          DateOfBirth =new DateTime(1958,3,7) },
                            new Employee{ FirstName="Bill", LastName="Gates", JobTitle=JobTitleType.Architect,
                                          DateOfBirth =new DateTime(1962,8,22) },
                            new Employee{ FirstName="John", LastName="Toshack", JobTitle=JobTitleType.Developer,
                                          DateOfBirth =new DateTime(1960,3,12) }
                        }
                    },
                    new Company
                    {
                        Name = "Apple",
                        EstablishmentYear = 1975,
                        Employees = new List<Employee>()
                        {
                            new Employee{ FirstName="Steve", LastName="Jobs", JobTitle=JobTitleType.Administrator,
                                          DateOfBirth =new DateTime(1978,3,7) },
                            new Employee{ FirstName="Red", LastName="Apple", JobTitle=JobTitleType.Developer,
                                          DateOfBirth =new DateTime(1977,8,22) },
                            new Employee{ FirstName="Lourie", LastName="Singer", JobTitle=JobTitleType.Manager,
                                          DateOfBirth =new DateTime(1975,3,12) }
                        }
                    }
                };

                context.Companies.AddRange(newCompanies);
                context.SaveChanges();
            }
        }
    }
}

