﻿
using System;
using Microsoft.Owin.Hosting;
using OwinSelfHost;

namespace Startup
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://localhost:9000/";

            using (WebApp.Start<OwinStartup>(url: baseAddress))
            {
                Console.WriteLine("Server started...");
                Console.WriteLine("Send request or press return key to shutdown the server...");

                Console.ReadLine();
                Console.WriteLine("Server stopped");
            }
        }
    }
}
