USE [CompaniesDB]
GO
/****** Object:  Table [dbo].[Companies]    Script Date: 2018-10-02 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[EstablishmentYear] [int] NULL,
 CONSTRAINT [PK_dbo.Companies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 2018-10-02 15:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[CompanyId] [int] NULL,
	[DateOfBirth] [datetime] NULL,
	[JobTitle] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Companies] ON 

INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (1, N'Tandeta 7', 1999)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (4, N'Chicago Bulls', 1972)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (5, N'IBM', 1935)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (6, N'Microsoft', 1967)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (7, N'Apple', 1975)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (8, N'Chicago Bulls', 1972)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (9, N'IBM', 1935)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (10, N'Microsoft', 1967)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (11, N'Apple', 1975)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (12, N'Tandeta 7', 1999)
INSERT [dbo].[Companies] ([Id], [Name], [EstablishmentYear]) VALUES (13, N'Tandeta 7', 1999)
SET IDENTITY_INSERT [dbo].[Companies] OFF
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (1, N'Yari', N'Litmannen', 1, CAST(N'1958-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (2, N'Harrison', N'Ford', 1, CAST(N'1958-03-08 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (7, N'Michael', N'Jordan', 4, CAST(N'1968-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (8, N'Isiah', N'Thomas', 4, CAST(N'1972-08-22 00:00:00.000' AS DateTime), N'Architect')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (9, N'Scottie', N'Pippen', 4, CAST(N'1970-03-12 00:00:00.000' AS DateTime), N'Manager')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (10, N'John', N'Thompson', 5, CAST(N'1938-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (11, N'Jason', N'Scott', 5, CAST(N'1932-08-22 00:00:00.000' AS DateTime), N'Architect')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (12, N'Crazy', N'Horse', 5, CAST(N'1930-03-12 00:00:00.000' AS DateTime), N'Manager')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (13, N'Steve', N'Ballmer', 6, CAST(N'1958-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (14, N'Bill', N'Gates', 6, CAST(N'1962-08-22 00:00:00.000' AS DateTime), N'Architect')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (15, N'John', N'Toshack', 6, CAST(N'1960-03-12 00:00:00.000' AS DateTime), N'Developer')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (16, N'Steve', N'Jobs', 7, CAST(N'1978-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (17, N'Red', N'Apple', 7, CAST(N'1977-08-22 00:00:00.000' AS DateTime), N'Developer')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (18, N'Lourie', N'Singer', 7, CAST(N'1975-03-12 00:00:00.000' AS DateTime), N'Manager')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (19, N'Michael', N'Jordan', 8, CAST(N'1968-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (20, N'Isiah', N'Thomas', 8, CAST(N'1972-08-22 00:00:00.000' AS DateTime), N'Architect')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (21, N'Scottie', N'Pippen', 8, CAST(N'1970-03-12 00:00:00.000' AS DateTime), N'Manager')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (22, N'John', N'Thompson', 9, CAST(N'1938-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (23, N'Jason', N'Scott', 9, CAST(N'1932-08-22 00:00:00.000' AS DateTime), N'Architect')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (24, N'Crazy', N'Horse', 9, CAST(N'1930-03-12 00:00:00.000' AS DateTime), N'Manager')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (25, N'Steve', N'Ballmer', 10, CAST(N'1958-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (26, N'Bill', N'Gates', 10, CAST(N'1962-08-22 00:00:00.000' AS DateTime), N'Architect')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (27, N'John', N'Toshack', 10, CAST(N'1960-03-12 00:00:00.000' AS DateTime), N'Developer')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (28, N'Steve', N'Jobs', 11, CAST(N'1978-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (29, N'Red', N'Apple', 11, CAST(N'1977-08-22 00:00:00.000' AS DateTime), N'Developer')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (30, N'Lourie', N'Singer', 11, CAST(N'1975-03-12 00:00:00.000' AS DateTime), N'Manager')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (31, N'Yari', N'Litmannen', 12, CAST(N'1958-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (32, N'Harrison', N'Ford', 12, CAST(N'1958-03-08 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (33, N'Yari', N'Litmannen', 13, CAST(N'1958-03-07 00:00:00.000' AS DateTime), N'Administrator')
INSERT [dbo].[Employees] ([Id], [FirstName], [LastName], [CompanyId], [DateOfBirth], [JobTitle]) VALUES (34, N'Harrison', N'Ford', 13, CAST(N'1958-03-08 00:00:00.000' AS DateTime), N'Administrator')
SET IDENTITY_INSERT [dbo].[Employees] OFF
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Employees_dbo.Companies_CompanyId] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_dbo.Employees_dbo.Companies_CompanyId]
GO
