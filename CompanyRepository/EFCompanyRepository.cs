﻿using CompanyRepository.Api;
using CompanyRepository.Models;
using System.Collections.Generic;
using System.Linq;
using CompanyRepository.DBContext;
using System.Data.Entity;


namespace CompanyRepository
{
    public class EFCompanyRepository : ICompanyRepository
    {
        private CompanyDbContext context = new CompanyDbContext();
        public List<Company> GetCompanyRepository()
        {
            return context.Companies.Include(c => c.Employees).ToList();
        }

        public DbContext GetDBContext()
        {
            return context;
        }
    }
}
