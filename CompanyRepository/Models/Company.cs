﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyRepository.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? EstablishmentYear { get; set; }
        public IList<Employee> Employees { get; set; }
    }
}
