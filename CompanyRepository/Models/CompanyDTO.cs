﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CompanyRepository.Models
{
    public class CompanyDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int? EstablishmentYear { get; set; }
        [Required]
        public IList<EmployeeDTO> Employees { get; set; }
    }
}
