﻿
namespace CompanyRepository.Models
{
    public enum JobTitleType
    {
        Administrator, Developer, Architect, Manager
    }
}
