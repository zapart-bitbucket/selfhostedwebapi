﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyRepository.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Company Company { get; set; }
        public int? CompanyId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [Column("JobTitle")]
        public string JobTitleTypeToString
        {
            get { return JobTitle.ToString(); }
            private set { JobTitle = value.ParseEnum<JobTitleType>(); }
        }

        [NotMapped]
        public JobTitleType JobTitle { get; set; }
    }
}
