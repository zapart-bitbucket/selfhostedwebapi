﻿using CompanyRepository.Api;
using CompanyRepository.DBContext;
using CompanyRepository.ExceptionHandlers;
using CompanyRepository.Models;
using System;
using System.Linq;

namespace CompanyRepository
{
    public class EFCompanyRepositoryCRUD : CompanyRepositoryCRUD
    {
        private CompanyDbContext _dbContext;

        public EFCompanyRepositoryCRUD(ICompanyRepository repo) : base(repo)
        {
            _dbContext = new CompanyDbContext();
        }

        public override long AddCompany(Company company)
        {
            if (company == null)
            {
                throw new CompanyRepositoryCompanyNullException();
            }

            _dbContext.Companies.Add(company);
            _dbContext.SaveChanges();
            return company.Id;
        }

        public override void DeleteCompany(long id)
        {
            Company companyToRemove = _dbContext.Companies.Find(id);
            if (companyToRemove != null)
            {
                var EmployeesToRemove = _dbContext.Employees.Where(e => e.CompanyId == companyToRemove.Id);
                foreach (var e in EmployeesToRemove)
                {
                    _dbContext.Employees.Remove(e);
                }
                _dbContext.Companies.Remove(companyToRemove);
                _dbContext.SaveChanges();
            }
            else
            {
                throw new ArgumentOutOfRangeException("Id", id, "");
            }
        }

        public override void UpdateCompany(long id, Company company)
        {
            if (company == null)
            {
                throw new CompanyRepositoryCompanyNullException();
            }

            Company companyToUpdate = _dbContext.Companies.Find(id);
            if (companyToUpdate != null)
            {
                companyToUpdate.Name = company.Name;
                companyToUpdate.EstablishmentYear = company.EstablishmentYear;
                companyToUpdate.Employees = company.Employees;
                _dbContext.SaveChanges();
            }
            else
            {
                throw new ArgumentOutOfRangeException("Id", id, "");  
            }
        }
    }
}