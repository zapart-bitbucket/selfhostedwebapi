﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompanyRepository.Models;
using CompanyRepository.Api;

namespace CompanyRepository
{
    public class CompanyRepositoryService
    {
        public ICompanyRepositoryCRUD CompaniesRepositoryCRUD { get; set; }

        public CompanyRepositoryService(ICompanyRepositoryCRUD repoCRUD)
        {
            CompaniesRepositoryCRUD = repoCRUD;
        }
    }
}
