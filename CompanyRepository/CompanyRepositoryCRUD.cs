﻿using System;
using System.Collections.Generic;
using System.Linq;
using CompanyRepository.Models;
using CompanyRepository.Api;
using CompanyRepository.ExceptionHandlers;

namespace CompanyRepository
{
    public class CompanyRepositoryCRUD : ICompanyRepositoryCRUD
    {
        private ICompanyRepository _repository;

        public CompanyRepositoryCRUD(ICompanyRepository repo)
        {
            _repository = repo;
        }

        public virtual long AddCompany(Company company)
        {
            if (company == null)
            {
                throw new CompanyRepositoryCompanyNullException();
            }

            _repository.GetCompanyRepository().Add(company);
            return company.Id;
        }

        public virtual void DeleteCompany(long id)
        {
            var companyToRemove = _repository.GetCompanyRepository().SingleOrDefault(c => c.Id == id);
            if (companyToRemove != null)
            {
                _repository.GetCompanyRepository().Remove(companyToRemove);
            }
            else
            {
                throw new ArgumentOutOfRangeException("Id", id, "");
            }
        }

        public virtual IEnumerable<Company> GetAllCompanies()
        {
            return _repository.GetCompanyRepository();
        }

        public virtual Company GetById(long id)
        {
            var companyById = _repository.GetCompanyRepository().SingleOrDefault(c => c.Id == id);
            if (companyById != null)
            {
                return companyById;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Id", id, "");
            }
        }

        public IEnumerable<Company> GetRepository()
        {
            return _repository.GetCompanyRepository();
        }

        public virtual IEnumerable<Company> SearchCompanyRepo(SearchCompanyDTO search)
        {
            if( search == null )
            {
                throw new CompanyRepositorySearchNullException(); 
            }

            var result = new List<Company>();
                       
            var fromKeyword = FindCompanyByKeyword(search.Keyword);
            if (fromKeyword != null)
            {
                result.AddRange(fromKeyword);
            }

            var fromDate = FindCompanyByEmployeeBirthDate(search.EmployeeDateOfBirthFrom, search.EmployeeDateOfBirthTo);
            if (fromDate != null)
            {
                result.AddRange(fromDate);
            }

            var fromJobTitles = FindCompanyByEmployeeJobTitle(search.EmployeeJobTitles);
            if (fromJobTitles != null)
            {
                result.AddRange(fromJobTitles);
            }
                
            return result.GroupBy(c => c.Name).Select(c => c.First()).ToList();
        }

        private IEnumerable<Company> FindCompanyByKeyword(String keyword)
        {
            if( keyword == null )
            {
                return null;
            }

            var keywordToLower = keyword.ToLower();
            return _repository.GetCompanyRepository()
                              .Where(c => c.Name.ToLower().Contains(keywordToLower) ||
                                          c.Employees.Any(e => e.FirstName.ToLower().Contains(keywordToLower)) ||
                                          c.Employees.Any(e => e.LastName.ToLower().Contains(keywordToLower)));
        }

        private IEnumerable<Company> FindCompanyByEmployeeBirthDate(DateTime? from, DateTime? to)
        {
            if (from != null && to != null)
            {
                if (from > to)
                {
                    var temp = from;
                    from = to;
                    to = temp;
                }
                return _repository.GetCompanyRepository()
                                  .Where(c => c.Employees.Any(e => e.DateOfBirth >= from) &&
                                              c.Employees.Any(e => e.DateOfBirth <= to));
            }

            if( from != null && to == null )
            {
                return _repository.GetCompanyRepository()
                                  .Where(c => c.Employees.Any(e => e.DateOfBirth >= from));
            }

            if (from == null && to != null)
            {
                return _repository.GetCompanyRepository()
                                  .Where(c => c.Employees.Any(e => e.DateOfBirth <= to));
            }

            return null;
        }

        private IEnumerable<Company> FindCompanyByEmployeeJobTitle(List<string> jobTitles)
        {
            if( jobTitles == null )
            {
                return null;
            }

            return _repository.GetCompanyRepository()
                              .Where(c => c.Employees.Any(e => jobTitles.Any(j => j.Contains(e.JobTitle.ToString()))));
        }

        public void SetRepository(ICompanyRepository repo)
        {
            _repository = repo;
        }

        public virtual void UpdateCompany(long id, Company company)
        {
            if( company == null )
            {
                throw new CompanyRepositoryCompanyNullException();
            }

            var companyToUpdate = _repository.GetCompanyRepository().SingleOrDefault(c => c.Id == id);
            if (companyToUpdate != null)
            {
                companyToUpdate.Name = company.Name;
                companyToUpdate.EstablishmentYear = company.EstablishmentYear;
                companyToUpdate.Employees = company.Employees;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Id", id, "");
            }
        }
    }
}
