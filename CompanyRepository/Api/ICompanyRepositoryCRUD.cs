﻿using System.Collections.Generic;
using CompanyRepository.Models;

namespace CompanyRepository.Api
{
    public interface ICompanyRepositoryCRUD
    {
        IEnumerable<Company> GetRepository();
        IEnumerable<Company> GetAllCompanies();
        IEnumerable<Company> SearchCompanyRepo(SearchCompanyDTO searchCompany);
        Company GetById(long id);
        long AddCompany(Company company);
        void UpdateCompany(long id, Company company);
        void DeleteCompany(long id);
    }
}
