﻿using System.Collections.Generic;
using CompanyRepository.Models;
namespace CompanyRepository.Api
{
    public interface ICompanyRepository
    {
        List<Company> GetCompanyRepository();
    }
}
