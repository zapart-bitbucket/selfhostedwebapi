﻿using System;
using System.Collections.Generic;
using CompanyRepository.Api;
using CompanyRepository.Models;

namespace CompanyRepository
{
    public class ByHandCompanyRepository : ICompanyRepository
    {
        private static readonly List<Company> repository = new List<Company>
        {
            new Company{ Id=0, Name="IBM", EstablishmentYear=1923, Employees=new List<Employee>
                {   //new Employee{ FirstName="Mickey", LastName="Mouse", DateOfBirth=new DateTime(1934,04,02), JobTitle=JobTitleType.Administrator },
                    //new Employee{ FirstName="Yogi", LastName="Bear", DateOfBirth=new DateTime(1935,05,03), JobTitle=JobTitleType.Architect},
                    new Employee{ FirstName="Fred", LastName="Flinstone", DateOfBirth=new DateTime(1936,06,04), JobTitle=JobTitleType.Developer },
                    new Employee{ FirstName="Barba", LastName="Papa", DateOfBirth=new DateTime(1937,07,05), JobTitle=JobTitleType.Manager }
                }
            },
            new Company{ Id=1, Name="Microsoft", EstablishmentYear=1923, Employees=new List<Employee>
                {   //new Employee{ FirstName="Cristiano", LastName="Ronaldo", DateOfBirth=new DateTime(1984,04,02), JobTitle=JobTitleType.Administrator },
                    //new Employee{ FirstName="Leo", LastName="Messi", DateOfBirth=new DateTime(1985,04,02), JobTitle=JobTitleType.Architect },
                    //new Employee{ FirstName="Edson", LastName="Pele", DateOfBirth=new DateTime(1986,04,02), JobTitle=JobTitleType.Developer },
                    new Employee{ FirstName="Huan", LastName="Mata", DateOfBirth=new DateTime(1987,04,02), JobTitle=JobTitleType.Manager }
                }
            },
            new Company{ Id=2, Name="Intel", EstablishmentYear=1923, Employees=new List<Employee>
                {   //new Employee{ FirstName="Stan", LastName="Lem", DateOfBirth=new DateTime(1954,04,02), JobTitle=JobTitleType.Administrator },
                    //new Employee{ FirstName="Phillip", LastName="Dick", DateOfBirth=new DateTime(1955,04,02), JobTitle=JobTitleType.Architect },
                    //new Employee{ FirstName="David", LastName="Brin", DateOfBirth=new DateTime(1956,04,02), JobTitle=JobTitleType.Developer },
                    new Employee{ FirstName="Arthur", LastName="Clarke", DateOfBirth=new DateTime(1957,04,02), JobTitle=JobTitleType.Manager }
                }
            }
        };

        public List<Company> GetCompanyRepository()
        {
            return repository;
        }
    }
}

