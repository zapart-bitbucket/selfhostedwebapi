﻿using CompanyRepository.Models;
using System.Data.Entity;

namespace CompanyRepository.DBContext
{
    public class CompanyDbContext : DbContext
    {
        public CompanyDbContext():base("CompaniesDB") {}
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
